# -*- coding: utf-8 -*-
"""
Created on Tue May 14 11:37:11 2019

@author: Alejandro Giraldo Martinez
@author: Pedrito Perez
"""

#import necessary library to load file and plot 
import numpy as np
import matplotlib.pyplot as plt 


data = np.loadtxt('data.txt',delimiter=',')#load values of .txt file
x = data[:,0]#Column 1 (time)
y1 = data[:,1]#Column 2 (Sample 1) 
#y2 = data[:,2]#Column 3 (Sample 2)
plt.plot(x,y1,'ro')#Plot Time/Sample 1
#plt.plot(x,y2,'g+')#Plot Time/Sample 2
plt.legend(['Sample 1','Sample 2'])#Name of samples in the graphic    
plt.title('DATA',loc='center')#Title of the graphic
plt.xlabel('Time [s]')#Title axis x
plt.ylabel('Values Sample 1')#Title axis y
plt.grid()#Show grid in the graphic
plt.show()#Show plot complete
